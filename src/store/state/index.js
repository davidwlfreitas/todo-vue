import { STORAGE_KEY } from '../../store/mutation-types'

export default {
    todos: JSON.parse(window.localStorage.getItem(STORAGE_KEY) || '[]'),
    filter: 'ALL',
    input: ''
}