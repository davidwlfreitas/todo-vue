import { STORAGE_KEY } from '../../store/mutation-types'

const plugins = [
    store => {
        store.subscribe((mutation, { todos }) => {
        window.localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
        })
    }
]

export default plugins