import { ADD_TODO, DELETE_TODO, EDIT_TODO, TOGGLE_TODO } from './mutation-types'

export const addTodo = ({commit}, title) => {
  commit(ADD_TODO, {
    title,
    done: false
  })
}

export const toggleTodo = ({commit}, todo) => {
    commit(TOGGLE_TODO, {
        todo,
        done: !todo.done
    })
}

export const editTodo = ({commit}, { todo, title }) => {
    commit(EDIT_TODO, { todo, title })
}

export const deleteTodo = ({commit}, todo) => {
  commit(DELETE_TODO, {todo})
}