import { ADD_TODO, DELETE_TODO, EDIT_TODO, TOGGLE_TODO } from './mutation-types'

export default {

  [TOGGLE_TODO] (state, { todo, done }) {
    todo.done = done
  },

  [ADD_TODO] (state, todo) {
    state.todos.push(todo)
  },

  [EDIT_TODO] (state, { todo, title }) {
    todo.title = title
  },

  [DELETE_TODO] (state, {todo}) {
    const todoIndex = state.todos.indexOf(todo)
    state.todos.splice(todoIndex, 1)
  }

}